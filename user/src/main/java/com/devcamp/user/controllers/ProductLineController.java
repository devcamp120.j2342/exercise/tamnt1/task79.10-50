package com.devcamp.user.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.user.exportData.ProductLineExcelExporter;
import com.devcamp.user.models.ProductLine;
import com.devcamp.user.repository.ProductLineRepository;
import com.devcamp.user.services.ProductLineService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class ProductLineController {
    private final ProductLineService productLineService;
    private final ProductLineRepository productLineRepository;

    public ProductLineController(ProductLineService productLineService, ProductLineRepository productLineRepository) {
        this.productLineService = productLineService;
        this.productLineRepository = productLineRepository;
    }

    @GetMapping("/product-lines")
    public ResponseEntity<List<ProductLine>> getAllProductLine(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<ProductLine> productLines = productLineService.getAllProductLines(page, size);
            return ResponseEntity.ok(productLines);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/product-lines/{id}")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable Integer id) {
        ProductLine productLine = productLineService.getProductLineById(id);
        if (productLine != null) {
            return ResponseEntity.ok(productLine);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/product-lines")
    public ResponseEntity<ProductLine> createProductLine(@RequestBody ProductLine productLine) {
        ProductLine createdProductLine = productLineService.createProductLine(productLine);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdProductLine);
    }

    @PutMapping("/product-lines/{id}")
    public ResponseEntity<ProductLine> updateProductLine(@PathVariable Integer id,
            @RequestBody ProductLine productLine) {
        ProductLine updatedProductLine = productLineService.updateProductLine(id, productLine);
        if (updatedProductLine != null) {
            return ResponseEntity.ok(updatedProductLine);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/product-lines/{id}")
    public ResponseEntity<Void> deleteProductLine(@PathVariable Integer id) {
        productLineService.deleteProductLine(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/export/product-lines/excel")
    public void exportProductLinesToExcel(HttpServletResponse response) throws IOException {

        List<ProductLine> productLines = new ArrayList<>();
        productLineRepository.findAll().forEach(productLines::add);

        ProductLineExcelExporter exporter = new ProductLineExcelExporter();
        exporter.export(productLines, response);
    }

}
