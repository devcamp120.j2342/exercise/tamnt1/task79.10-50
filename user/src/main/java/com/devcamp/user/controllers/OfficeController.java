package com.devcamp.user.controllers;

import com.devcamp.user.exportData.OfficeExcelExporter;
import com.devcamp.user.models.Office;
import com.devcamp.user.repository.OfficeRepository;
import com.devcamp.user.services.OfficeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class OfficeController {
    private final OfficeService officeService;
    private OfficeRepository officeRepository;

    public OfficeController(OfficeService officeService, OfficeRepository officeRepository) {
        this.officeService = officeService;
        this.officeRepository = officeRepository;
    }

    @GetMapping("/offices")
    public ResponseEntity<List<Office>> getAllOffice(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            List<Office> office = officeService.getAllOffices(page, size);
            return ResponseEntity.ok(office);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("offices/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable int id) {
        Office office = officeService.getOfficeById(id);
        if (office != null) {
            return ResponseEntity.ok(office);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/offices")
    public ResponseEntity<Office> createOffice(@RequestBody Office office) {
        Office createdOffice = officeService.createOffice(office);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdOffice);
    }

    @PutMapping("offices/{id}")
    public ResponseEntity<Office> updateOffice(@PathVariable int id, @RequestBody Office office) {
        Office updatedOffice = officeService.updateOffice(id, office);
        if (updatedOffice != null) {
            return ResponseEntity.ok(updatedOffice);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("offices/{id}")
    public ResponseEntity<Void> deleteOffice(@PathVariable int id) {
        officeService.deleteOffice(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/export/offices/excel")
    public void exportOfficesToExcel(HttpServletResponse response) throws IOException {

        List<Office> offices = new ArrayList<>();
        officeRepository.findAll().forEach(offices::add);

        OfficeExcelExporter exporter = new OfficeExcelExporter();
        exporter.export(offices, response);
    }

}
