package com.devcamp.user.exportData;

import java.util.Date;
import java.util.List;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;

import com.devcamp.user.models.Order;

public class OrderExcelExporter extends AbstractExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    public OrderExcelExporter() {
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Orders");
        XSSFRow row = sheet.createRow(0);

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        cellStyle.setFont(font);

        createCell(row, 0, "ID", cellStyle);
        createCell(row, 1, "Comments", cellStyle);
        createCell(row, 2, "Order Date", cellStyle);
        createCell(row, 3, "Required Date", cellStyle);
        createCell(row, 4, "Shipped Date", cellStyle);
        createCell(row, 5, "Status", cellStyle);
        createCell(row, 6, "Customer ID", cellStyle);
    }

    private void createCell(XSSFRow row, int columnIndex, Object value, CellStyle style) {
        XSSFCell cell = row.createCell(columnIndex);
        sheet.autoSizeColumn(columnIndex);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }

        cell.setCellStyle(style);
    }

    public void export(List<Order> orders, HttpServletResponse response) throws IOException {
        super.setResponseHeader(response, "application/octet-stream", ".xlsx");

        writeHeaderLine();
        writeDataLines(orders);

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
    }

    private void writeDataLines(List<Order> orderList) {
        int rowIndex = 1;

        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        cellStyle.setFont(font);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        for (Order order : orderList) {
            XSSFRow row = sheet.createRow(rowIndex++);
            int columnIndex = 0;
            createCell(row, columnIndex++, order.getId(), cellStyle);
            createCell(row, columnIndex++, order.getComments(), cellStyle);

            Date orderDate = order.getOrderDate();
            if (orderDate != null) {
                createCell(row, columnIndex++, dateFormat.format(orderDate), cellStyle);
            } else {
                createCell(row, columnIndex++, "", cellStyle);
            }

            Date requiredDate = order.getRequiredDate();
            if (requiredDate != null) {
                createCell(row, columnIndex++, dateFormat.format(requiredDate), cellStyle);
            } else {
                createCell(row, columnIndex++, "", cellStyle);
            }

            Date shippedDate = order.getShippedDate();
            if (shippedDate != null) {
                createCell(row, columnIndex++, dateFormat.format(shippedDate), cellStyle);
            } else {
                createCell(row, columnIndex++, "", cellStyle);
            }

            createCell(row, columnIndex++, order.getStatus(), cellStyle);
            createCell(row, columnIndex++, order.getCustomer().getId(), cellStyle);
        }
    }

}
