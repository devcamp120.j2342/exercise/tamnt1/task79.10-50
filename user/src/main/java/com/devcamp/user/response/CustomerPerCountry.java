package com.devcamp.user.response;

public class CustomerPerCountry {
    private String country;
    private int customerPerCountry;

    public CustomerPerCountry() {
    }

    public CustomerPerCountry(String country, int customerPerCountry) {
        this.country = country;
        this.customerPerCountry = customerPerCountry;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCustomerPerCountry() {
        return customerPerCountry;
    }

    public void setCustomerPerCountry(int customerPerCountry) {
        this.customerPerCountry = customerPerCountry;
    }

}
