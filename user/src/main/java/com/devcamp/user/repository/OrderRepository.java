package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.user.models.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

}