package com.devcamp.user.services;

import com.devcamp.user.models.Payment;
import com.devcamp.user.repository.PaymentRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;

    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public List<Payment> getAllPayments(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Payment> paymentPage = paymentRepository.findAll(pageable);
        return paymentPage.getContent();
    }

    public Payment getPaymentById(int id) {
        Optional<Payment> optionalPayment = paymentRepository.findById(id);
        return optionalPayment.orElse(null);
    }

    public Payment createPayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    public Payment updatePayment(int id, Payment payment) {
        Optional<Payment> optionalPayment = paymentRepository.findById(id);
        if (optionalPayment.isPresent()) {
            Payment existingPayment = optionalPayment.get();
            existingPayment.setCheckNumber(payment.getCheckNumber());
            existingPayment.setPaymentDate(payment.getPaymentDate());
            existingPayment.setAmmount(payment.getAmmount());
            return paymentRepository.save(existingPayment);
        } else {
            return null;
        }
    }

    public void deletePayment(int id) {
        paymentRepository.deleteById(id);
    }
}
