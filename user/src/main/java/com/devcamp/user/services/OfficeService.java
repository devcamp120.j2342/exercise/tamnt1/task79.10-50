package com.devcamp.user.services;

import java.util.List;
import java.util.Optional;
import com.devcamp.user.models.Office;
import com.devcamp.user.repository.OfficeRepository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class OfficeService {
    private final OfficeRepository officeRepository;

    public OfficeService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public List<Office> getAllOffices(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Office> officePage = officeRepository.findAll(pageable);
        return officePage.getContent();
    }

    public Office getOfficeById(int id) {
        Optional<Office> optionalOffice = officeRepository.findById(id);
        return optionalOffice.orElse(null);
    }

    public Office createOffice(Office office) {
        return officeRepository.save(office);
    }

    public Office updateOffice(int id, Office office) {
        Optional<Office> optionalOffice = officeRepository.findById(id);
        if (optionalOffice.isPresent()) {
            Office existingOffice = optionalOffice.get();
            existingOffice.setCity(office.getCity());
            existingOffice.setPhone(office.getPhone());
            existingOffice.setAddressLine(office.getAddressLine());
            existingOffice.setState(office.getState());
            existingOffice.setCountry(office.getCountry());
            existingOffice.setTerritory(office.getTerritory());
            return officeRepository.save(existingOffice);
        } else {
            return null;
        }
    }

    public void deleteOffice(int id) {
        officeRepository.deleteById(id);
    }
}
